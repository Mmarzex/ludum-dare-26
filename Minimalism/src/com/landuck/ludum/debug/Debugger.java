package com.landuck.ludum.debug;

public class Debugger {

	public static boolean isDebug = false;

	public static void log(String message) {
		if (isDebug)
			System.out.println("[debug] " + message);
	}

	public static void setDebug(boolean debug) {
		isDebug = debug;
	}

}
