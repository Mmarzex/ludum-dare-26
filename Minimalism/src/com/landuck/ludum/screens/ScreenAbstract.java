package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.landuck.ludum.Core;

public abstract class ScreenAbstract implements Screen {

	protected final Core core;
	protected static Stage stage;
	
	private SpriteBatch batch;
	private BitmapFont font;
	private Table table;
	private Skin skin;

	public ScreenAbstract(Core core) {
		this.core = core;
		ScreenAbstract.stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		
		stage.act(delta);
		
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		
		stage.draw();

	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		if(font != null)
			font.dispose();
		if(batch != null)
			batch.dispose();
	
		Core.manager.clear();
	}
	
	public BitmapFont getFont() {
		if(font == null) {
			font = new BitmapFont(Gdx.files.internal("data/ui/fnt.fnt"), false);
		}
		return font;
	}
	
	public SpriteBatch getBatch() {
		if(batch == null) {
			batch = new SpriteBatch();
		}
		return batch;
	}

	public Table getTable() {
		if(table == null) {
			table = new Table(getSkin());
			table.setFillParent(true);
			stage.addActor(table);
		}
		return table;
	}
	
	public Skin getSkin() {
		
		if(skin == null) {
			skin = new Skin();
			
			Pixmap p = new Pixmap(1, 1, Format.RGBA8888);
			p.setColor(Color.WHITE);
			p.fill();
			skin.add("white", new Texture(p));
			skin.add("default", getFont());
			
			// button
			TextButtonStyle tbs = new TextButtonStyle();
			tbs.up = skin.newDrawable("white", Color.ORANGE);
			tbs.down = skin.newDrawable("white", Color.ORANGE);
			tbs.checked = skin.newDrawable("white", Color.BLACK);
			tbs.over = skin.newDrawable("white", Color.WHITE);
			tbs.font = skin.getFont("default");
			skin.add("default", tbs);
			
			LabelStyle ls = new LabelStyle();
			ls.font = skin.getFont("default");
			skin.add("default", ls);
			
		}
		
		return skin;
	}
	
	
	public Core getGame() {
		return core;
	}

}
