package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.landuck.ludum.Core;
import com.landuck.ludum.debug.Debugger;

public class ScreenLoad extends ScreenAbstract {
	
	private float percent;
	
	private Texture tex;
	private Image img;

	public ScreenLoad(Core core) {
		super(core);

	}

	@Override
	public void show() {
		super.show();
		
		Core.manager.load("data/game/bg.png", Texture.class);	
		Core.manager.finishLoading();
		
		tex = Core.manager.get("data/game/bg.png");
		img = new Image(tex);
		img.setFillParent(true);
		stage.addActor(img);
		
		
		//Load assets here
		Core.manager.load("data/game/player.png", Texture.class);
		Core.manager.load("data/game/vert.png", Texture.class);
		Core.manager.load("data/game/hor.png", Texture.class);
		
	}

	@Override
	public void render(float delta) {
		super.render(delta);
		
		getBatch().begin();
			getFont().drawMultiLine(getBatch(), "LOADING\n ", Gdx.graphics.getWidth() / 2 -95, Gdx.graphics.getHeight() / 2);
		getBatch().end();
		
		if(Core.manager.update()) {
				getGame().setScreen(new ScreenGame(getGame()));
		}
		
		percent = Interpolation.linear.apply(percent, Core.manager.getProgress(), 0.1f);
		if(percent >= 0.98) {
			percent = 1.0f;
		}
		
		Debugger.log("Loaded: " + percent);

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();

	}
}
