package com.landuck.ludum.screens;

import box2dLight.DirectionalLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.landuck.ludum.Core;
import com.landuck.ludum.debug.Debugger;
import com.landuck.ludum.entities.EnemyManager;
import com.landuck.ludum.entities.Player;
import com.landuck.ludum.game.EnemyCollision;

public class ScreenGame extends ScreenAbstract {
	
	//World
	private World world;
	private Box2DDebugRenderer debug;
	private RayHandler rh;
	private DirectionalLight worldLightD;
	private DirectionalLight worldLightU;
	private static boolean isLose = false;

	//Player
	private float score = 0;
	private Player player;
	
	//Enemies
	private EnemyManager em;
	
	// DIFFUCUTLY CONTROLS
	public static float scoreTime = 0.0f;
	public static float timePassed = 0.0f;
	public static float speedMulti = 1.0f;
	public static float spawnMulti = 1.0f;
	public static float timeIncrement = 0.0f;
	
	// LOSE
	private Texture bg;
	private static Image bgI;

	public ScreenGame(Core core) {
		super(core);

	}

	@Override
	public void show() {
		super.show();
		
		
		bg = Core.manager.get("data/game/bg.png");
		bgI = new Image(bg);
		bgI.setFillParent(true);
	
		
		world = new World(new Vector2(0.0f, 0.0f), true);
		world.setContactListener(new EnemyCollision());
		debug = new Box2DDebugRenderer();
		debug.setDrawVelocities(true);
		rh = new RayHandler(world);
		rh.setCombinedMatrix(stage.getCamera().combined);
		//rh.setAmbientLight(Color.ORANGE.r, Color.ORANGE.g, Color.ORANGE.b, 1.0f);
		rh.setAmbientLight(0.0f, 0.0f, 0.0f, 1.0f);
		//rh.setBlur(true);
		worldLightD = new DirectionalLight(rh, 500, Color.CYAN, 270);
		worldLightU = new DirectionalLight(rh, 500, Color.ORANGE, 90);

		
		em = new EnemyManager(world, rh);
		
		player = new Player(world, rh);
	}

	@Override
	public void render(float delta) {
		
		
		timePassed += delta;
		scoreTime += delta;
		
		// GAME

		if(!isLose) {
		
		world.step(delta, 3, 3);
		
	
		super.render(delta);	
		
		em.update(delta, getBatch());
		
		
		player.updateAndRender(delta, getBatch());
		
		rh.setCombinedMatrix(stage.getCamera().combined, stage.getCamera().position.x, stage.getCamera().position.y, stage.getCamera().viewportWidth, stage.getCamera().viewportHeight);
		rh.updateAndRender();
		
		
		//debug.render(world, stage.getCamera().combined);	
		
		// UI
		
		
		getBatch().begin();
			getFont().draw(getBatch(), "SCORE: " + score, Gdx.graphics.getWidth() - 450, stage.getHeight() - getFont().getLineHeight());
		getBatch().end();
		
		
		// LOGIC
		
		if(scoreTime >= 1) {
			score += 50;
			scoreTime = 0;
		}

			if (timePassed >= 10) {
				Debugger.log("DIFFUCULTY INCREASED!");
				speedMulti += 0.5;
				score += 100;
				timePassed = 0;
				em.upDifficutly();
				if (score >= 1000) {
					changeColors();
					em.waveActive = false;
					if (score >= 2000) {
						em.waveActive = false;
						if (score >= 3000) {
							em.waveActive = false;
							if (score >= 5000) {
								em.waveActive = false;
								if (score >= 7000) {
									em.waveActive = false;
								}
							}

						}

					}
				}
			}

		} else {
			
			
			getBatch().begin();
			getFont().drawMultiLine(getBatch(), "   YOU LOST\n  Score: " + score + "\n(click to retry)", Gdx.graphics.getWidth() / 2 - 205, stage.getHeight() / 2);
			getBatch().end();
			
			if(Gdx.input.isTouched()) {
				isLose = false;
				getGame().setScreen(new ScreenGame(getGame()));
			
			}
			
		}
		
		
	}

	public void changeColors() {

		worldLightD.setColor(getRandomColor());
		worldLightU.setColor(getRandomColor());
		
		if(score > 1500) {
			//rh.setAmbientLight(getRandomColor());
		}

	}
	
	public Color getRandomColor() {
		
		switch(getNumber(1, 6)) {
		case 1:
			return Color.CYAN;
		case 2:
			return Color.RED;
		case 3:
			return Color.ORANGE;
		case 4:
			return Color.BLUE;
		case 5:
			return Color.YELLOW;
		
		}
		return Color.ORANGE;
	}
	
	public int getNumber(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));
	}
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
		
		rh.dispose();
	
	}
	
	public static void lose() {
		stage.addActor(bgI);
		isLose = true;
	}

}
