package com.landuck.ludum.screens;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.landuck.ludum.Core;

public class ScreenHowTo extends ScreenAbstract {
	
	private Texture bg;
	private Image bgI;

	public ScreenHowTo(Core core) {
		super(core);

	}

	@Override
	public void show() {
		super.show();
		
		Core.manager.load("data/game/bg.png", Texture.class);
		Core.manager.finishLoading();
		
		bg = Core.manager.get("data/game/bg.png");
		bgI = new Image(bg);
		bgI.setFillParent(true);
		stage.addActor(bgI);
	
		getTable().getColor().a = 0.0f;
		getTable().addAction(Actions.fadeIn(1.5f));
		
		TextButton back = new TextButton("Back", getSkin());
		back.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getGame().setScreen(new ScreenMenu(getGame()));
			}
		});
		
	
		back.align(Align.bottom | Align.right);
		
		getTable().add(back).size(250, 50);
		getFont().scale(0.1f);

	}

	@Override
	public void render(float delta) {
		super.render(delta);
		
		getBatch().begin();
		getFont().drawMultiLine(getBatch(), "Avoid all obstacles and go\nfor the highest score.\nUse the light beams to\nanticipate coming obstacles\n\n\n\nThis game was created for\nLudum Dare 26 \nTwitter: @_TylerChurchill", stage.getWidth() / 2 - 425, stage.getHeight() - 125);
		getBatch().end();


	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();

		Core.manager.clear();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();

	}

}
