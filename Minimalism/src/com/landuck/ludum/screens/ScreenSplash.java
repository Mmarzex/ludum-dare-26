package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.landuck.ludum.Core;

public class ScreenSplash extends ScreenAbstract {

	private Image splashImage;
	private Texture splashTexture;

	public ScreenSplash(Core core) {
		super(core);

	}

	@Override
	public void show() {
		super.show();
		
		stage.addActor(getSpalshActor());

	}

	@Override
	public void render(float delta) {
		super.render(delta);

		if (Gdx.input.isTouched()) {
			getGame().setScreen(new ScreenMenu(getGame()));
		}

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();

		Core.manager.clear();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();

	}

	public Actor getSpalshActor() {
		Core.manager.load("data/splash/splash.png", Texture.class);
		Core.manager.finishLoading();

		splashTexture = Core.manager.get("data/splash/splash.png");
		splashImage = new Image(splashTexture);

		splashImage.setPosition(stage.getWidth() / 2 - splashImage.getWidth()
				/ 2, stage.getHeight() / 2 - splashImage.getHeight() / 2);
		splashImage.getColor().a = 0.0f;
		splashImage.addAction(Actions.sequence(Actions.fadeIn(0.75f),
				Actions.delay(1.0f), Actions.fadeOut(1.0f), new Action() {
					public boolean act(float delta) {
						getGame().setScreen(new ScreenMenu(getGame()));
						return true;
					}
				}));
		return splashImage;
	}
}
