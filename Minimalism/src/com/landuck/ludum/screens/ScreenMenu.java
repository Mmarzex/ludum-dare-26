package com.landuck.ludum.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.landuck.ludum.Core;

public class ScreenMenu extends ScreenAbstract {
	
	private Texture bg;
	private Image bgI;
	private Sound click;

	public ScreenMenu(Core core) {
		super(core);
	}
 
	@Override
	public void show() {
		super.show();
		
		Core.manager.load("data/game/bg.png", Texture.class);
		Core.manager.finishLoading();
		
		click = Gdx.audio.newSound(Gdx.files.internal("data/game/click.wav"));
		
		bg = Core.manager.get("data/game/bg.png");
		bgI = new Image(bg);
		bgI.setFillParent(true);
		
		stage.addActor(bgI);

		getTable().getColor().a = 0.0f;
		getTable().addAction(Actions.fadeIn(1.5f));
		
		TextButton start = new TextButton("Start", getSkin());
		start.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				click.play();
				getGame().setScreen(new ScreenLoad(getGame()));
			}
		});
		
		TextButton how = new TextButton("Rules", getSkin());
		how.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				click.play();
				getGame().setScreen(new ScreenHowTo(getGame()));
			}
		});
		
		TextButton exit = new TextButton("Quit", getSkin());
		exit.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				click.play();
				Gdx.app.exit();
			}
		});
		
		getTable().add("ILLUMBOX").top();
		getTable().row();
		getTable().add(start).size(250, 50).uniform().padTop(150).padRight(10).spaceBottom(25);
		getTable().add(how).size(250, 50).uniform().padTop(150).padRight(10).spaceBottom(25);
		getTable().add(exit).size(250, 50).uniform().padTop(150).padLeft(10).spaceBottom(25);
		
	}

	@Override
	public void render(float delta) {
		super.render(delta);

	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void hide() {
		super.hide();
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();

	}

}
