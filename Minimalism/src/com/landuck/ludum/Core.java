package com.landuck.ludum;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.landuck.ludum.debug.Debugger;
import com.landuck.ludum.screens.ScreenLoad;
import com.landuck.ludum.screens.ScreenSplash;

public class Core extends Game {

	public static AssetManager manager = new AssetManager();
	public static int UNIT_SCALE = 32;
	
	Music music;

	public Core() {

		
		Debugger.log(Color.ORANGE.r + "" + Color.ORANGE.g + "" + Color.ORANGE.b);

	}

	@Override
	public void create() {
		Debugger.log("Game started");
		music = Gdx.audio.newMusic(Gdx.files.internal("data/game/ms.mp3"));
		music.play();
		music.setLooping(true);
		
		if (Debugger.isDebug) {
			setScreen(new ScreenLoad(this));
		} else {
			setScreen(new ScreenSplash(this));
		}
	}

}
