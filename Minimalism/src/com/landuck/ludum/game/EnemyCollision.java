package com.landuck.ludum.game;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.landuck.ludum.screens.ScreenGame;

public class EnemyCollision implements ContactListener {
	

	@Override
	public void beginContact(Contact contact) {
		
		if(contact.getFixtureA().getBody().getUserData() == "player" && contact.getFixtureB().getBody().getUserData() == "enemy") {
			ScreenGame.lose();
		}
		
		if(contact.getFixtureA().getBody().getUserData() == "enemy" && contact.getFixtureB().getBody().getUserData() == "player") {
			ScreenGame.lose();
		}
	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		//contact.setEnabled(false);
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
