package com.landuck.ludum.entities;

import aurelienribon.bodyeditor.BodyEditorLoader;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.landuck.ludum.Core;
import com.landuck.ludum.debug.Debugger;

public class Player extends Entity {
	
	private Vector2 origin;
	//private PointLight light;
	
	public float speed = 64.0f;

	public Player(World world, RayHandler rh) {
		

		
		BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/game/entities.json"));
	
		texture = Core.manager.get("data/game/player.png");
		
		sprite = new Sprite(texture);
		sprite.setColor(color);
		
		BodyDef bd = new BodyDef();
		bd.type = BodyDef.BodyType.DynamicBody;
		bd.position.set(Gdx.graphics.getWidth() / 2 + 16, Gdx.graphics.getHeight() / 2 - 16);
		bd.fixedRotation = true;
		
		
		
		FixtureDef f = new FixtureDef();
		f.density = 0.05f;
	
	
		
		body = world.createBody(bd);
		body.setUserData("player");
		body.setLinearDamping(0.3f);
		body.setAngularDamping(1.0f);
		
	
		
		loader.attachFixture(body, "player", f, 32);
		origin = loader.getOrigin("player", 32).cpy();
	}

	@Override
	public void updateAndRender(float delta, SpriteBatch batch) {
		Vector2 pos = body.getPosition().sub(origin);
		

		if(Gdx.input.isKeyPressed(Keys.W)) {
			body.applyLinearImpulse(new Vector2(0.0f, speed), pos);
		}
		
		if(Gdx.input.isKeyPressed(Keys.S)) {
			body.applyLinearImpulse(new Vector2(0.0f, -speed), pos);
		}
		
		if(Gdx.input.isKeyPressed(Keys.A)) {
			body.applyLinearImpulse(new Vector2(-speed, 0.0f), pos);
		}
		
		if(Gdx.input.isKeyPressed(Keys.D)) {
			body.applyLinearImpulse(new Vector2(speed, 0.0f), pos);
		}
		
	
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {

			body.applyAngularImpulse(-35.0f);
		
			Debugger.log("RIGHT");

		}

		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			body.applyAngularImpulse(35.0f);
			Debugger.log("LEFT");

		}
		
		
		sprite.setPosition(pos.x, pos.y);
		sprite.setOrigin(origin.x, origin.y);
		sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
		
		batch.begin();
			sprite.draw(batch);
		batch.end();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean flagDead() {
		// TODO Auto-generated method stub
		return false;
	}

}
