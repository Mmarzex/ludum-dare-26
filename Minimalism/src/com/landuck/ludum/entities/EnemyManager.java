package com.landuck.ludum.entities;

import java.util.ArrayList;
import java.util.Iterator;

import aurelienribon.bodyeditor.BodyEditorLoader;
import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.landuck.ludum.screens.ScreenGame;

public class EnemyManager {

	private World world;
	private RayHandler rh;

	private ArrayList<Enemy> current = new ArrayList<Enemy>();
	//private ArrayList<Enemy> remove = new ArrayList<Enemy>();
	BodyEditorLoader loader = new BodyEditorLoader(Gdx.files.internal("data/game/entities.json"));

	private float time = 0.0f;
	private float frequency = 0.9f;
	//private int timeOut = 9;
	
	public boolean waveActive = true;

	public EnemyManager(World world, RayHandler rh) {
		this.world = world;
		this.rh = rh;
		
		sendWave();
	}

	public void update(float delta, SpriteBatch batch) {

		time += delta;

		if (waveActive) {
			if (time >= frequency * 5) {
				sendWave();
				time = 0;
			}

		} else {
			if (time >= frequency) {
				current.add(getEnemy());
				time = 0;
			}
		}


		for (Enemy e : current) {
			e.updateAndRender(delta, batch);
		}

	}
	

	public void sweepDeadBodies() {
		for (Iterator<Body> iter = world.getBodies(); iter.hasNext();) {
			Body body = iter.next();
			if (body != null) {
				if (body.getUserData() == null) {
					world.destroyBody(body);
					body.setUserData(null);
					body = null;
				}
			}
		}
	}
	

	public Enemy getEnemy() {

		switch (getNumber(1, 4)) {
		case 1:
			// Debugger.log("Created RIGHT");
			Right r = new Right(Gdx.graphics.getWidth() + 50, getLocHor(),
					loader, world, rh);
			r.speed = ScreenGame.speedMulti * getNumber(2, 32);
			return r;

		case 2:
			// Debugger.log("Created TOP");
			Top t = new Top(getLocVert(), Gdx.graphics.getHeight() + 100,
					loader, world, rh);
			t.speed = ScreenGame.speedMulti * getNumber(15, 32);
			return t;
		case 3:
			// Debugger.log("Created LEFT");
			Left l = new Left(0 - 80, getLocHor(), loader, world, rh);
			l.speed = ScreenGame.speedMulti * getNumber(15, 32);
			return l;
		case 4:
			// Debugger.log("Created BOTTOM");
			Bottom b = new Bottom(getLocVert(), 0, loader, world, rh);
			b.speed = ScreenGame.speedMulti * getNumber(15, 32);
			return b;

		}
		return null;

	}
	
	public void sendWave() {
		
		int x = (Gdx.graphics.getWidth() / 32) + 2;
		
		switch (getNumber(1, 2)) {
		case 1:
			for (int i = 0; i < getNumber(15, 30); i++) {
				Top t = new Top(32 * i, Gdx.graphics.getHeight() + 100 + (32 * i), loader, world, rh);
				t.speed = 48;
				current.add(t);
			}
			
		case 2:
			for(int i = 0; i < x - getNumber(6, 30); i++) {
				Top t = new Top(Gdx.graphics.getWidth() - 32 * i, Gdx.graphics.getHeight() + 100 + (32 * i), loader, world, rh);
				t.speed = 32;
				current.add(t);
			}
		}
	}

	public void setFrequency(int f) {
		this.frequency = f;
	}

	public int getNumber(int min, int max) {
		return min + (int) (Math.random() * ((max - min) + 1));

	}

	public int getLoc(int min, int max) {
		int x = min + (int) (Math.random() * ((max - min) + 1));
		if (x % 32 == 0) {
			return x;
		} else {
			x += x % 32;
		}
		return x;
	}

	public int getLocVert() {
		int d = Gdx.graphics.getWidth() / 32;

		int x = getNumber(0, d);

		return x * 32;
	}

	public int getLocHor() {
		int d = Gdx.graphics.getHeight() / 32;

		int x = getNumber(0, d);

		return x * 32;
	}

	public void upDifficutly() {
		if (frequency - 0.1f > 0.3) {
			frequency -= 0.1f;
		} else {
			frequency -= 0.03f;
		}

	}

}
