package com.landuck.ludum.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.landuck.ludum.debug.Debugger;

public abstract class Entity {

	public Body body;
	public Texture texture;
	public Sprite sprite;
	public Vector2 position = new Vector2();
	public Color color = new Color(Color.WHITE);
	
	public float lifeTime = 0.0f;

	public Entity() {
		Debugger.log("Created entity");
	}

	public abstract void updateAndRender(float delta, SpriteBatch batch);
	
	public abstract void dispose();
	
	public abstract boolean flagDead();

	public Vector2 getPosition() {
		return this.position;
	}

	public Body getBody() {
		return this.body;
	}

	public void setColor(Color color) {
		this.color = color;
		this.sprite.setColor(color);
	}

	public Texture getTexture() {
		if (texture != null) {
			return texture;
		}
		Debugger.log("no texture set for entity");
		return null;
	}
	
	public void setTexture(Texture texture) {
		this.texture = texture;
	}

	public Color getColor() {
		return color;
	}

}
