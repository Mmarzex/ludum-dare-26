package com.landuck.ludum.entities;

import aurelienribon.bodyeditor.BodyEditorLoader;
import box2dLight.RayHandler;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.landuck.ludum.Core;

public class Left extends Enemy {


	private float death = 0.0f;
	public float speed = 0.0f;

	Vector2 origin;


	
	public Left(float x, float y, BodyEditorLoader l, World world, RayHandler rh) {
		super(x, y, l, world, rh);

		texture = Core.manager.get("data/game/hor.png", Texture.class);
		
		sprite = new Sprite(texture);
		
		
		BodyDef bd = new BodyDef();
		bd.type = BodyDef.BodyType.DynamicBody;
		bd.position.set(x, y);
		bd.fixedRotation = true;
		
		FixtureDef d = new FixtureDef();
		d.density = 0.05f;
		d.isSensor = true;
		
		body = world.createBody(bd);
		body.setUserData("enemy");
		
		l.attachFixture(body, "hor", d, 160);
		origin = l.getOrigin("hor", 160).cpy();
		

		
	}

	@Override
	public void updateAndRender(float delta, SpriteBatch batch) {
		Vector2 pos = body.getPosition().sub(origin);
		
		body.applyLinearImpulse(new Vector2(speed, 0.0f), pos);
		
		sprite.setPosition(pos.x, pos.y);
		sprite.setOrigin(origin.x, origin.y);
		sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
		
		batch.begin();
			sprite.draw(batch);
		batch.end();
		
	}

	@Override
	public void dispose() {

	}

	public boolean flagDead() {
		return ((lifeTime >= death) ? false : true);
	}

}
